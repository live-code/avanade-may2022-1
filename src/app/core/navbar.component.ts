import { Component, OnInit } from '@angular/core';
import { ThemeService } from './services/theme.service';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-navbar',
  template: `
    
    <div 
      class="nav" 
      [ngClass]="{
        'my-dark': themeService.isDark(),
        light: themeService.theme === 'light'
      }"
    >
      <button routerLinkActive="active"
              routerLink="/"
              [routerLinkActiveOptions]="{ exact: true }"
      >Hello</button>
      <button routerLinkActive="active" routerLink="login">login</button>
      <button routerLinkActive="active" routerLink="users">users</button>
      <button  *ngIf="authService.isLogged()"  routerLinkActive="active" routerLink="contacts">contacts</button>
      <button routerLinkActive="active" routerLink="http">http</button>
      <button routerLinkActive="active" routerLink="tvmaze">tvmaze</button>
      <button routerLinkActive="active" routerLink="settings">settings</button>
      <button routerLinkActive="active" routerLink="uikit">UIKIT</button>
      <button routerLinkActive="active" routerLink="uikit2">UIKIT2</button>
      <button *ngIf="authService.isLogged()" 
              (click)="authService.logout()">Logout</button>
      
      
      <div class="bg">
        {{authService.data?.displayName}}
      </div>

    </div>
  `,
  styles: [`
    .active {
      background-color: orange;
    }
    .nav {
      padding: 20px;
    }
    .my-dark {
      color: white;
      background-color: #222;
    }
    .light {
      background-color: gray;
    }
  `]
})
export class NavbarComponent implements OnInit {
  constructor(
    public themeService: ThemeService,
    public authService: AuthService
  ) {
  }

  ngOnInit(): void {
  }
}

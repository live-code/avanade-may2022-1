import { Injectable } from '@angular/core';
import { Auth } from '../../model/auth';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data: Auth | null = null;

  constructor(private http: HttpClient, private router: Router) {

    const dataFromLocalStorage = localStorage.getItem('auth')
    if (dataFromLocalStorage) {
      this.data = JSON.parse(dataFromLocalStorage) as Auth;
    }
  }

  login({ user, pass }: any) {
    // this.http.get<Auth>(`http://localhost:3000/login?user=${formData.user}&pass=${formData.pass}`)
    const params = new HttpParams()
      .set('user', user)
      .set('password', pass)

    this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .subscribe(res => {
        this.data = res;
        this.router.navigateByUrl('contacts')
        localStorage.setItem('auth', JSON.stringify(res))
      })
  }

  logout() {
    this.data = null;
    this.router.navigateByUrl('login')
    localStorage.removeItem('auth')
  }

  isLogged(): boolean {
    return !!this.data
  }



}

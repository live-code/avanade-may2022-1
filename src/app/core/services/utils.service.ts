import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  doSomething(value: number) {
    return 10 * value;
  }
}

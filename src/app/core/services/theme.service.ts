import { Injectable } from '@angular/core';
export type Theme = 'dark' | 'light';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private _value: Theme = 'dark'

  constructor() {
    const theme: Theme | null = localStorage.getItem('theme') as Theme
    if (theme) {
      this._value = theme
    }
  }

  set theme(theme: Theme) {
    localStorage.setItem('theme', theme)
    this._value = theme;
  }

  get theme() {
    return this._value
  }

  isDark(): boolean {
    return this._value === 'dark'
  }
}

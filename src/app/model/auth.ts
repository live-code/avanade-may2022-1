export interface Auth {
  displayName: string;
  token: string;
  role: string;
}

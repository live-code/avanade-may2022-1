
export type Gender = 'M' | 'F';

export interface User {
  id: number;
  name: string;
  age: number;
  gender: Gender;
  city: string;
  birthday: number;
  bitcoins: number;
}

export type userForm = Pick<User, 'name' | 'city'>;

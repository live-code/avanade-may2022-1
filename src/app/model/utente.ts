export interface Utente {
  id: number;
  name: string;
  city: string;
  desc: string;
}

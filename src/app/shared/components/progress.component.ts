import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  template: `
    <i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>
  `,
})
export class ProgressComponent {
}

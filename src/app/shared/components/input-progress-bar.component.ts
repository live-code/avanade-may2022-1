import { Component, Input, OnInit } from '@angular/core';
import { min } from 'rxjs';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-input-progress-bar',
  template: `
    <div
      class="my-progress-bar"
      [style.width.%]="getPerc()"
      [style.background-color]="color"
    ></div>
  `,
  styles: [`
    .my-progress-bar {
      width: 100%;
      height: 6px;
      transition: 1.5s width cubic-bezier(0.68, -0.6, 0.32, 1.6);
    }
  `]
})
export class InputProgressBarComponent  {
  @Input() input: NgModel | undefined;
  @Input() color = 'purple'

  getPerc() {
    const minLengthError = this.input?.errors?.['minlength']
    if (minLengthError) {
      return (minLengthError.actualLength / minLengthError.requiredLength) * 100
    }
    return 0
  }
}

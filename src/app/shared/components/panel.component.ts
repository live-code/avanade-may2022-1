import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-panel',
  template: `
    <div class="card">
      <div 
        [class]="'card-header ' + headerCls"
        [ngClass]="{'bg-warning': this.isError} "
        (click)="isOpen = !isOpen"
      >
        {{title}}
        <div class="pull-right" *ngIf="icon">
          <i [class]="icon" 
             (click)="iconClick.emit()"></i>
        </div>
      </div>
      
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class PanelComponent  {
  @Input() title: string | null = null;
  @Input() headerCls: string = '';
  @Input() isError: boolean = false;
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter()
  isOpen = true;
}

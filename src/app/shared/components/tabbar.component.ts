import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Country } from '../../model/tv-maze-series';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item"
          *ngFor="let item of items"
          (click)="tabClick.emit(item)"
      >
        <a class="nav-link" 
           [ngClass]="{'active': item.id === active?.id}">
          {{item.name}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: any[] = []
  @Input() active: any;
  @Output() tabClick = new EventEmitter<any>();
}

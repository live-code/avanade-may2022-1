import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    <img 
      *ngIf="value"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + value + ' &size=600,400&zoom=' + zoom" alt="">
  `,
})
export class StaticMapComponent  {
  @Input() value: string | undefined = 'Rome';
  @Input() zoom: number = 10;
}

import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  template: `
    <h1 [style.color]="color" class="bg">
      Hello {{myName}}
    </h1>
  `,
  styles: [`
   
  `]
})
export class HelloWorldComponent {
  @Input() myName: string | null = null;
  @Input() color: string = 'red'
}

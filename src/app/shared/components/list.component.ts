import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-list',
  template: `
    <li 
      class="list-group-item"
      *ngFor="let item of items"
      [ngClass]="{active: item.id === active?.id}"
      (click)="itemClick.emit(item)"
    >
      {{item.name}}
      
      <div class="pull-right" *ngIf="icon">
        <i [class]="icon"
           (click)="iconClickHandler($event, item)"></i>
      </div>
    </li>
  `
})
export class ListComponent {
  @Input() items: any[] = [];
  @Input() icon: string | undefined;
  @Input() active: any;
  @Output() itemClick = new EventEmitter<any>()
  @Output() iconClick = new EventEmitter<any>()

  iconClickHandler(event: MouseEvent, item: any) {
    event.stopPropagation();
    this.iconClick.emit(item)
  }
}

import { Component } from '@angular/core';

export type Page = 'hello' | 'users' | 'contacts' | 'demo-http' | 'tvmaze';

@Component({
  selector: 'app-root',
  template: `
    <div class="container mt-4">
      <app-navbar></app-navbar>
      <hr>
      <router-outlet></router-outlet>
    </div>
  `,

})
export class AppComponent  {
}



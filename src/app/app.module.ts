import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// core
import { AppComponent } from './app.component';
import { NavbarComponent } from './core/navbar.component';

// features
import { HelloComponent } from './features/hello/hello.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { UsersComponent } from './features/users/users.component';
import { DemoHttpComponent } from './features/http/demo-http.component';
import { TvmazeComponent } from './features/tvmaze/tvmaze.component';
import { ContactsDetailsComponent } from './features/contacts-details/contacts-details.component';
import { SettingsComponent } from './features/settings/settings.component';
import { LoginComponent } from './features/login/login.component';
import { AuthGuard } from './core/services/auth.guard';
import { HelloWorldComponent } from './shared/components/hello-world.component';
import { UikitComponent } from './features/uikit/uikit.component';
import { PanelComponent } from './shared/components/panel.component';
import { TabbarComponent } from './shared/components/tabbar.component';
import { Uikit2Component } from './features/uikit/uikit2.component';
import { TabContentComponent } from './features/uikit/components/tab-content.component';
import { StaticMapComponent } from './shared/components/static-map.component';
import { TvmazeSearchFormComponent } from './features/tvmaze/components/tvmaze-search-form.component';
import { TvmazeListComponent } from './features/tvmaze/components/tvmaze-list.component';
import { ListComponent } from './shared/components/list.component';
import { TvmazeModalComponent } from './features/tvmaze/components/tvmaze-modal.component';
import { InputProgressBarComponent } from './shared/components/input-progress-bar.component';
import { ProgressComponent } from './shared/components/progress.component';
import { ContactsFormComponent } from './features/contacts/components/contacts-form.component';
import { ContactsListComponent } from './features/contacts/components/contacts-list.component';
import { ContactsListItemComponent } from './features/contacts/components/contacts-list-item.component';

@NgModule({
  declarations: [
    // core
    AppComponent,
    NavbarComponent,
    // features
    HelloComponent,
    ContactsComponent,
    UsersComponent,
    DemoHttpComponent,
    TvmazeComponent,
    ContactsDetailsComponent,
    SettingsComponent,
    LoginComponent,
    UikitComponent,
    Uikit2Component,
      TabContentComponent,
    // shared
    HelloWorldComponent,
    PanelComponent,
    TabbarComponent,
    StaticMapComponent,
    TvmazeSearchFormComponent,
    TvmazeListComponent,
    ListComponent,
    TvmazeModalComponent,
    InputProgressBarComponent,
    ProgressComponent,
    ContactsFormComponent,
    ContactsListComponent,
    ContactsListItemComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      /*{ path: 'home', component: HelloComponent },*/
      { path: 'login', component: LoginComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'contacts', component: ContactsComponent, canActivate: [AuthGuard] },
      { path: 'contacts/:id', component: ContactsDetailsComponent },
      { path: 'users', component: UsersComponent },
      { path: 'http', component: DemoHttpComponent },
      { path: 'tvmaze', component: TvmazeComponent },
      { path: 'uikit', component: UikitComponent },
      { path: 'uikit2', component: Uikit2Component },
      { path: '', component: HelloComponent},
      { path: '**', redirectTo: ''}
    ])

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

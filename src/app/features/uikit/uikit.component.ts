import { Component, OnInit } from '@angular/core';
import { HelloComponent } from '../hello/hello.component';
import { PanelComponent } from '../../shared/components/panel.component';

@Component({
  selector: 'app-uikit',
  template: `
    <app-hello-world color="blue"></app-hello-world>
    <button (click)="doSomething()">1</button>
    <button (click)="openUrl('http://www.google.com')">2</button>
    <button (click)="openUrl('http://www.facebook.com')">3</button>

    <app-panel
      title="Facebook profile"
      icon="fa fa-facebook"
      (iconClick)="openUrl('http://www.facebook.com')"
      [isError]="true"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur debitis deleniti eius expedita nisi nostrum officiis sit suscipit voluptates voluptatibus! Aperiam architecto cum dolore enim error magnam maiores perspiciatis veniam.
    </app-panel>
    
    <br>
    <app-panel
      headerCls="bg-danger text-white"
      icon="fa fa-bluetooth"
      (iconClick)="visible = !visible"
      [title]="yourname" 
    >
      <app-hello-world [myName]="yourname" color="red"></app-hello-world>
      <button (click)="yourname = 'ciccio'">change name</button>
    </app-panel>
    
    <app-panel title="container">
      <div class="row">
        <div class="col">
          <app-panel title="left"></app-panel>
        </div>
        <div class="col">
          <app-panel title="right"></app-panel>
        </div>
      </div>
    </app-panel>
    
    <h1 *ngIf="visible">Hello world</h1>

    
  `,
})
export class UikitComponent {
  yourname: string | null = 'Pippo';
  visible = false;

  doSomething() {
    console.log('do something')
  }

  openUrl(url: string) {
    window.open(url)
  }
}

import { Component, OnInit } from '@angular/core';
import { HelloComponent } from '../hello/hello.component';
import { PanelComponent } from '../../shared/components/panel.component';

export interface Country {
  id: number;
  name: string;
  cities: City[]
}
export interface City {
  id: number;
  name: string;
  desc: string;
}

@Component({
  selector: 'app-uikit2',
  template: `

    <app-list
      [items]="countries"
      icon="fa fa-trash"
      (iconClick)="deleteCountry($event)"
    ></app-list>

    
    <div class="row">
      <div class="col">
        <app-list
          [items]="countries"
          icon="fa fa-link"
          [active]="activeCountry"
          (itemClick)="selectedCountryHandler($event)"
          (iconClick)="openUrl($event)"
        ></app-list>
      </div>
      <div class="col">
        <app-list
          *ngIf="activeCountry"
          [items]="activeCountry.cities"
        ></app-list>
      </div>
      
    </div>
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <app-tabbar 
      [items]="countries"
      [active]="activeCountry"
      (tabClick)="selectedCountryHandler($event)"
    ></app-tabbar>
    
    <app-tabbar 
      *ngIf="activeCountry"
      [active]="activeCity"
      [items]="activeCountry.cities"
      (tabClick)="selectCityHandler($event)"
    ></app-tabbar>

    <app-city-content [city]="activeCity"></app-city-content>
    
    <!--
    <app-panel
      *ngIf="activeCountry" 
      [title]="activeCountry.name"
    >
      {{activeCountry.desc}}
    </app-panel>-->
  `,
})
export class Uikit2Component {
  countries: Country[] = [];
  activeCountry: Country | null = null;
  activeCity: City | null = null;
  zoomLevel = 5;

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 11,
          name: 'japan',
          cities: [
            { id: 1, name: 'Tokyo', desc: 'tokyo bla bla'},
            { id: 2, name: 'Kyoto', desc: 'kyoto bla bla'},
          ],

        },
        {
          id: 22, name: 'italy', cities: [
            { id: 21, name: 'Milan', desc: 'Milan bla bla' },
            { id: 22, name: 'Rome', desc: 'Rome bla bla' },
            { id: 34, name: 'Palermo', desc: 'Palermo bla bla' },
          ],
        },
        {
          id: 33, name: 'germany', cities: [
            { id: 1123, name: 'Berlin', desc: 'Berlin bla bla' },
          ],
        }
      ];
      this.selectedCountryHandler(this.countries[0])
    }, 600)
  }


  selectedCountryHandler(item: Country) {
    console.log('select country')
    this.activeCountry = item;
    this.activeCity = this.activeCountry.cities[0];
  }

  selectCityHandler(city: City) {
    this.activeCity = city;
  }

  deleteCountry(item: Country) {
    const index = this.countries.findIndex(c => c.id === item.id)
    this.countries.splice(index, 1)
  }

  openUrl(item: Country) {
    console.log('open url')
    // window.open(`https://en.wikipedia.org/wiki/${item.name}`)
  }
}

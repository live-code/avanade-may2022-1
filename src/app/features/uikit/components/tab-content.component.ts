import { Component, Input } from '@angular/core';
import { City } from '../uikit2.component';

@Component({
  selector: 'app-city-content',
  template: `

    <div *ngIf="city">
      <h1>{{city.name}}</h1>
      <app-static-map [zoom]="12" [value]="city.name"></app-static-map>
      <p>{{city.desc}}</p>
    </div>
  `
})
export class TabContentComponent {
  @Input() city: City | null = null;
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Utente } from '../../model/utente';
import { combineLatest, map } from 'rxjs';

@Component({
  selector: 'app-contacts-details',
  template: `
    <div class="alert alert" *ngIf="error">ahia!</div>
    <h1>{{id}}</h1> / {{index}}
    
    <div *ngIf="!error">
      <h1> {{user?.name}}</h1>
      <div> {{user?.desc}}</div>
      <img
        width="100%"
        style="height: 50px"
        [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + user?.city+ '&size=600,400'" alt="">
      
      <button (click)="gotoNext()">Go to next user</button>
    </div>

    <button routerLink="/contacts">Back to contacts</button>
  `,
})
export class ContactsDetailsComponent {
  user: Utente | undefined;
  error: boolean = false;
  id!: number

  ids: number[] = [];
  index = 0;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {

    combineLatest({
      params: this.activatedRoute.params,
      users: this.http.get<Utente[]>('http://localhost:3000/users')
    })
      .pipe(
        map(res => {
          return {
            routerId: +res.params['id'],
            ids: res.users.map(u => u.id),
          }
        })
      )
      .subscribe(res => {
        this.ids = res.ids;
        this.index = res.ids.findIndex(id => id === res.routerId)
        this.getData(res.routerId)
      })

  }

  getData(id: number) {
    this.http.get<Utente>(`http://localhost:3000/users/${id}`)
      .subscribe(
        res => {
          this.user = res;
        },
        err => this.error = true
      )
  }


  gotoNext() {
    if(this.index < this.ids.length - 1 ) {
      this.index = this.index + 1;
    } else {
      this.index = 0
    }
    const nextId = this.ids[this.index]
    this.router.navigateByUrl('/contacts/' + nextId )
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Utente } from '../../model/utente';

@Component({
  selector: 'app-contacts-details',
  template: `
    <div class="alert alert" *ngIf="error">ahia!</div>
    <h1>{{id}}</h1>
    
    <div *ngIf="!error">
      <h1> {{user?.name}}</h1>
      <div> {{user?.desc}}</div>
      <img
        width="100%"
        style="height: 50px"
        [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + user?.city+ '&size=600,400'" alt="">
      
      <button (click)="gotoNext()">Go to next user</button>
    </div>

    <button routerLink="/contacts">Back to contacts</button>
  `,
})
export class ContactsDetailsComponentImperativo {
  user: Utente | undefined;
  id: number | undefined;
  error: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {
    this.id = activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.getData(this.id)
    }
  }

  getData(id: number) {
    this.http.get<Utente>(`http://localhost:3000/users/${id}`)
      .subscribe(
        res => {
          this.user = res;
        },
        err => this.error = true
      )
  }

  gotoNext() {
    if (this.id) {
      const nextId = ++this.id;
      this.router.navigateByUrl('/contacts/' + nextId);
      this.getData(nextId)
    }

  }

}

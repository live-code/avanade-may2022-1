import { Injectable } from '@angular/core';
import { Post } from '../../../model/post';
import { HttpClient } from '@angular/common/http';

const API = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class HttpDemoService {
  posts: Post[] = [];
  error: boolean = false;

  constructor(private http: HttpClient ) {}

  getAllPosts() {
    this.http.get<Post[]>(`${API}/postss`)
      .subscribe(
        res => {
          this.posts = res;
        },
        err => {
          this.error = true;
        }
      )
  }

  deletePost(postId: number) {
    this.http.delete(`${API}/posts/${postId}`)
      .subscribe(() => {
        const index = this.posts.findIndex(p => p.id === postId)
        this.posts.splice(index, 1)
      })
  }
}

import { Component } from '@angular/core';
import { Post } from '../../model/post';
import { HttpDemoService } from './services/http-demo.service';

@Component({
  selector: 'app-demo-http',
  template: `
    demo http
    <div class="alert alert-danger"
         *ngIf="service.error">
      errore!!!
    </div>
    
    <li *ngFor="let post of service.posts">
      {{post.title}}
      <i class="fa fa-trash"
         (click)="service.deletePost(post.id)"></i>
    </li>
  `
})
export class DemoHttpComponent {
  constructor( public service: HttpDemoService) {
    service.getAllPosts()
  }
}

import { Component } from '@angular/core';
import { User } from '../../model/user';

const API = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';


@Component({
  selector: 'app-users',
  template: `
    <div class="alert alert-danger" *ngIf="!users.length"> non ci sono elementi </div>

    <input 
      type="text" class="form-control" placeholder="Name"
      (keyup.enter)="save(inputName, inputCity)"
      #inputName
    >
    
    <input 
      type="text" class="form-control" placeholder="City"
      (keyup.enter)="save(inputName, inputCity)"
      #inputCity
    >
    
    <div class="container mt-3" *ngIf="users.length">
      <h1>Ci sono {{users.length}} utenti</h1>
      
      <!--USERS LIST-->
      <li
        *ngFor="let user of users" 
        class="list-group-item"
        [ngClass]="{active: user.id === activeUser?.id}"
        (click)="selectUserHandler(user)"
      >
        <div class="d-flex justify-content-between align-items-center">
          <div class="d-flex align-items-center gap-3">
            <i
              class="fa"
              [ngClass]="{ 'male fa-mars': user.gender === 'M', 'female fa-venus': user.gender === 'F' }"
            ></i>
            
            <div>{{user.name }}</div>
          </div>
          
          <div>
            <i class="fa fa-trash" (click)="deleteUser(user)"></i>
          </div>
        </div>
      </li>
    </div>
    
    <!--MODAL USER DETAILS-->
    <div *ngIf="activeUser" class="my-modal text-center">
      <h1>{{activeUser.name}}</h1>
      <img *ngIf="activeUser.city" [src]="getCity(activeUser)" alt="" width="100%">
      <button class="btn btn-primary mt-2" (click)="closeModal()">CLOSE</button>
    </div>
  `,
})
export class UsersComponent  {
  users: Partial<User>[] = [
    {
      id: 11,
      name: 'fabio',
      age: 16,
      gender: 'M',
      city: 'Palermo',
      birthday: 1598910648000,
      bitcoins: 2.11878327874327
    },
    {
      id: 22,
      name: 'Mario',
      age: 22,
      gender: 'M',
      city: 'Roma',
      birthday: 1548910648000,
      bitcoins: 2.11878327874327
    },
    {
      id: 31,
      name: 'Paola',
      age: 44,
      gender: 'F',
      city: 'Trieste',
      birthday: 1598910648000,
      bitcoins: 2.11878327874327
    }
  ]
  activeUser: Partial<User> | null = null;

  save(inputName: HTMLInputElement, inputCity: HTMLInputElement) {
    const user: Partial<User> = { id: Date.now(), name: inputName.value, city: inputCity.value}
    this.users.push( user );
    inputName.value = '';
    inputCity.value = '';
    inputName.focus()
  }

  selectUserHandler(user: Partial<User>) {
    this.activeUser = user;
  }

  closeModal() {
    this.activeUser = null;
  }

  deleteUser(userToRemove: Partial<User>) {
    const index = this.users.findIndex(item => item.id ===  userToRemove.id);
    this.users.splice(index, 1)
  }
  getCity(user: Partial<User>): string {
    return `${API}&center=${user.city}&size=400,200&zoom=10`
  }

  addUser() {
    this.users.push({
      id: Date.now(), // < ===
      name: 'name' + Math.random(),
      age: 222,
      gender: 'M',
      city: 'Palermo',
      birthday: 1598910648000,
      bitcoins: 2.11878327874327
    })
  }

}



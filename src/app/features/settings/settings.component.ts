import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { UtilsService } from '../../core/services/utils.service';

@Component({
  selector: 'app-settings',
  template: `
    <p>
      settings works! {{themeService.theme}}
    </p>
    
    <button (click)="themeService.theme = 'dark'">change theme to dark</button>
    <button (click)="themeService.theme = 'light'">change theme to light</button>
    <button>change lang to IT</button>
    <button>change lang to EN</button>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(
    public themeService: ThemeService,
    public utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    console.log(this.utilsService.doSomething(123))
  }

}

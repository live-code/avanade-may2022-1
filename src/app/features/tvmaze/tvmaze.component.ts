import { Component } from '@angular/core';
import { TvMazeService } from './services/tv-maze.service';

@Component({
  selector: 'app-tvmaze',
  template: `
    <app-tvmaze-search-form (search)="srv.search($event)"></app-tvmaze-search-form>
    <app-tvmaze-list [items]="srv.list" (clickSeries)="srv.seriesClickHandler($event)"></app-tvmaze-list>    
    <app-tvmaze-modal *ngIf="srv.currentShow" [show]="srv.currentShow" (close)="srv.currentShow = null"></app-tvmaze-modal>
  `,
})
export class TvmazeComponent {

  constructor(public srv: TvMazeService) {}

}

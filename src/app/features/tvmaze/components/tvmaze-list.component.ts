import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TvMazeService } from '../services/tv-maze.service';
import { Show, TvMazeSeries } from '../../../model/tv-maze-series';

@Component({
  selector: 'app-tvmaze-list',
  template: `
    <div class="grid">
      <div *ngFor="let s of items" class="movie"
           (click)="clickSeries.emit(s.show)">
        <img *ngIf="s.show.image" [src]="s.show.image?.medium" width="100%">
        <div class="noImage" *ngIf="!s.show.image"></div>
        <div class="movieText">{{s.show.name}}</div>
      </div>
    </div>
  `,
  styleUrls: [`./tvmaze-list.component.css`]
})
export class TvmazeListComponent  {
  @Input() items:  TvMazeSeries[] = [];
  @Output() clickSeries = new EventEmitter<Show>()

}

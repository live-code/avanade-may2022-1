import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TvMazeService } from '../services/tv-maze.service';

@Component({
  selector: 'app-tvmaze-search-form',
  template: `
    <form #f="ngForm" (submit)="search.emit(f.value.text)">
      <input type="text" ngModel name="text" required>
      <button hidden [disabled]="f.invalid">SAVE</button>
    </form>
  `,
})
export class TvmazeSearchFormComponent {
  @Output() search = new EventEmitter<string>()
}

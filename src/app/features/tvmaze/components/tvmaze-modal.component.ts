import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TvMazeService } from '../services/tv-maze.service';
import { Show } from '../../../model/tv-maze-series';

@Component({
  selector: 'app-tvmaze-modal',
  template: `
    <div class="my-modal">
      <div class="content">
        <h1>{{show?.name}}</h1>
        <img *ngIf="show?.image" [src]="show?.image?.original" width="100%">
        <div [innerHTML]="show?.summary"></div>

        <div class="tag" *ngFor="let g of show?.genres">{{g}}</div>

        <hr>
        <button (click)="close.emit()">CLOSE</button>
      </div>
    </div>
  `,
  styleUrls: [`./tvmaze-modal.component.css`]

})
export class TvmazeModalComponent {
  @Input() show: Show | null = null;
  @Output() close = new EventEmitter()

}

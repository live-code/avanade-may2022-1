import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Show, TvMazeSeries } from '../../../model/tv-maze-series';

@Injectable({
  providedIn: 'root'
})
export class TvMazeService {
  list: TvMazeSeries[] = [];
  currentShow: Show | null = null;

  constructor(private http: HttpClient) { }

  search(value: string) {
    this.http.get<TvMazeSeries[]>('https://api.tvmaze.com/search/shows?q=' + value)
      .subscribe(res => {
        this.list = res;
      })
  }

  seriesClickHandler(show: Show) {
    console.log(show)
    this.currentShow = show;
  }
}

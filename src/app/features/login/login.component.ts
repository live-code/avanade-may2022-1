import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from '../../model/auth';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-login',
  template: `
    
    <form #f="ngForm" 
          (submit)="authService.login(f.value)">
      <input type="text" ngModel name="user">
      <input type="password" ngModel name="pass">
      <button type="submit">LOGIN</button>
    </form>
  `,
  styles: [
  ]
})
export class LoginComponent  {

  constructor(public authService: AuthService) { }


}

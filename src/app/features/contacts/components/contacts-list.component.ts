import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ContactsService } from '../services/contacts.service';
import { Utente } from '../../../model/utente';

@Component({
  selector: 'app-contacts-list',
  template: `
    <li
      *ngFor="let user of users"
      (click)="itemClick.emit(user)"
      [style.background-color]="user.id === activeUser?.id ? 'red' : null"
    >
      {{user.name}}
      - {{user.city}}

      <i class="fa fa-trash" (click)="trashIconClick.emit(user)"></i>
      <i class="fa fa-link ms-2" [routerLink]="'/contacts/' + user.id"></i>

    </li>
  `,
  styles: [
  ]
})
export class ContactsListComponent  {
  @Input() users: Utente[] = []
  @Input() activeUser: Utente | undefined;
  @Output() itemClick = new EventEmitter<Utente>();
  @Output() trashIconClick = new EventEmitter<Utente>();
}

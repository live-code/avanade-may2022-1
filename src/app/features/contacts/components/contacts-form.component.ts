import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ContactsService } from '../services/contacts.service';
import { NgForm } from '@angular/forms';
import { Utente } from '../../../model/utente';

@Component({
  selector: 'app-contacts-form',
  template: `
    <form #f="ngForm"
          (submit)="save.emit(f.value)"
          >
      <div class="alert alert-danger" *ngIf="inputName.errors && inputName.dirty">
        <div *ngIf="inputName.errors?.['required']">il campo è obbligatorio</div>
        <div *ngIf="inputName.errors?.['minlength']">
          too short. Missing {{inputName.errors?.['minlength']?.['requiredLength'] - inputName.errors?.['minlength']?.['actualLength']}} chars
        </div>
      </div>
      <input
        class="form-control"
        [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid}"
        type="text"
        #inputName="ngModel"
        placeholder="Name"
        [ngModel]="active?.name"
        name="name"
        required minlength="3"
      >

      <app-input-progress-bar [input]="inputName" color="red"></app-input-progress-bar>

      <br>

      <div class="alert alert-danger" *ngIf="inputCity.errors && inputCity.dirty">
        <div *ngIf="inputCity.errors?.['required']">required</div>
        <div *ngIf="inputCity.errors?.['minlength']">
          too short. Missing {{inputCity.errors?.['minlength']?.['requiredLength'] - inputCity.errors?.['minlength']?.['actualLength']}} chars
        </div>
      </div>
      <input
        class="form-control"
        [ngClass]="{'is-invalid': inputCity.invalid && f.dirty, 'is-valid': inputCity.valid}"
        type="text"
        name="city"
        required minlength="5"
        placeholder="City"
        [ngModel]="active?.city"
        #inputCity="ngModel"
      >

      <app-input-progress-bar [input]="inputCity"></app-input-progress-bar>

      <button type="submit" [disabled]="f.invalid">save</button>
      <button type="button" (click)="resetForm(f)">reset</button>
    </form>
  `,
})
export class ContactsFormComponent  {
  @Input() active: Utente | undefined;
  @Output() save = new EventEmitter();
  @Output() reset = new EventEmitter();


  resetForm(f: NgForm) {
    this.reset.emit();
    f.reset();
  }
}

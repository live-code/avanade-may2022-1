import { Injectable } from '@angular/core';
import { Utente } from '../../../model/utente';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { delay, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  activeUser: any = null;
  users: Utente[] = []
  pending = false;

  constructor(private http: HttpClient) {}

  getAll() {
    this.pending = true;

    // ---------------o >
    // delay(Time)
    // -------------------o >
    this.http.get<Utente[]>('http://localhost:3000/users')
      .pipe(
        delay(1000)
      )
      .subscribe(res => {
        this.users = res;
        this.pending = false;
      })

  }

  save(formData: any) {
    if (this.activeUser?.id) {
      this.edit(formData);
    } else {
      this.add(formData)
    }
  }

  add(formData: any) {
    this.pending = true;
    this.http.post<Utente>('http://localhost:3000/users', formData)
      .pipe(
        delay(2000)
      )
      .subscribe(res => {
        this.pending = false;
        this.users.push(res)
      })
  }

  edit(formData: any) {
    this.http.patch<Utente>('http://localhost:3000/users/' + this.activeUser?.id, formData)
      .subscribe(res => {
        const index = this.users.findIndex(u => u.id === this.activeUser?.id)
        // this.users[index] = {...this.users[index], ...form.value}
        this.users[index] = res;
      })

  }


  deleteUser(userToDelete: Utente) {
    this.http.delete(`http://localhost:3000/users/${userToDelete.id}`)
      .subscribe(() => {
        const index = this.users.findIndex(u => u.id === userToDelete.id)
        this.users.splice(index, 1);
        if (userToDelete.id === this.activeUser?.id) {
          this.activeUser = null;
        }
      })
  }

  selectUser(user: Utente | null) {
    this.activeUser = user;
  }

  resetForm() {
    this.activeUser = null;
  }
}

import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Post } from '../../model/post';
import { Utente } from '../../model/utente';
import { ContactsService } from './services/contacts.service';


@Component({
  selector: 'app-contacts',
  template: `
    
    <app-contacts-form
      [active]="contactService.activeUser"
      (save)="contactService.save($event)"
      (reset)="contactService.resetForm()"
    ></app-contacts-form>

    <app-progress *ngIf="contactService.pending"></app-progress>

    <app-contacts-list 
      [users]="contactService.users"
      [activeUser]="contactService.activeUser"
      (itemClick)="contactService.selectUser($event)"
      (trashIconClick)="contactService.deleteUser($event)"
    ></app-contacts-list>
  `,
})
export class ContactsComponent {
  regex = /^[a-z0-9]+$/i;

  constructor(public contactService: ContactsService) {
    contactService.getAll();
  }




  ngOnDestroy() {
    this.contactService.selectUser(null)
  }
}

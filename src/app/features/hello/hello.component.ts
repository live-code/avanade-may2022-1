import { Component, Directive } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-hello',
  template: `
    {{label | json}}
    <form #f="ngForm" (submit)="save(f)">
      <input type="text" [ngModel]="label" name="city" required>
      <button type="submit">SAVE</button>
      <button type="button" (click)="reset()">reset</button>
    </form>
  `
})
export class HelloComponent {
  label: string | null = 'ciao';

  reset() {
    this.label = null;
  }
  save(f: NgForm) {
    console.log('save', f.value)
  }
}
